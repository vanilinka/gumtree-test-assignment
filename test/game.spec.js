import Game from '../src/js/game';

describe('Game', () => {
  describe("regular", () => {
    const rules = [{shape: "Rock", beats: ["Scissors"]},
      {shape: "Scissors", beats: ["Paper"]},
      {shape: "Paper", beats: ["Rock"]}];
    let game;

    before(() => {
      game = new Game(rules);
    });

    it('can find a winner', function() {
      expect(game.findWinner(rules[0], "Paper")).to.equal('player');
      expect(game.findWinner(rules[0], "Scissors")).to.equal('computer');
      expect(game.findWinner(rules[0], "Rock")).to.equal('tie');

      expect(game.findWinner(rules[1], "Paper")).to.equal('computer');
      expect(game.findWinner(rules[1], "Scissors")).to.equal('tie');
      expect(game.findWinner(rules[1], "Rock")).to.equal('player');

      expect(game.findWinner(rules[2], "Paper")).to.equal('tie');
      expect(game.findWinner(rules[2], "Scissors")).to.equal('player');
      expect(game.findWinner(rules[2], "Rock")).to.equal('computer');
    });
  });

  describe("extended", () => {
    const rules =[
      {shape: "Rock", beats: ["Scissors", "Lizard"]},
      {shape: "Scissors", beats: ["Paper", "Lizard"]},
      {shape: "Paper", beats: ["Rock", "Spock"]},
      {shape: "Spock", beats: ["Scissors", "Rock"]},
      {shape: "Lizard", beats: ["Paper", "Spock"]}
      ];

      let game;

      before(() => {
        game = new Game(rules);
      });

      it('can find a winner', function() {
        expect(game.findWinner(rules[3], "Paper")).to.equal('player');
        expect(game.findWinner(rules[3], "Scissors")).to.equal('computer');
        expect(game.findWinner(rules[3], "Rock")).to.equal('computer');
        expect(game.findWinner(rules[3], "Lizard")).to.equal('player');
        expect(game.findWinner(rules[3], "Spock")).to.equal('tie');

        expect(game.findWinner(rules[4], "Paper")).to.equal('computer');
        expect(game.findWinner(rules[4], "Scissors")).to.equal('player');
        expect(game.findWinner(rules[4], "Rock")).to.equal('player');
        expect(game.findWinner(rules[4], "Lizard")).to.equal('tie');
        expect(game.findWinner(rules[4], "Spock")).to.equal('computer');
    });
  })
});