import GameDOM from './game-DOM';

export default class Game {
  constructor(rules) {
    this.rules = rules;
    this.state = {
      score: {
        computer: 0,
        player: 0
      }
    }
  }

  start() {
    GameDOM.renderOnScreen(this.rules);
    GameDOM.updateScoreOnScreen(this.state.score);
    GameDOM.subscribeToEvents({
      play: this.play.bind(this),
      resetGame: this.resetGame.bind(this),
      generateComputerGame: this.generateComputerGame.bind(this)
    });
  }

  play(playerShape) {
    let computerChoice = this.rules[this.generateComputerAnswer()]; 
    let winner = this.findWinner(computerChoice, playerShape.trim());
    setTimeout(() => {
      GameDOM.updateMessageContainer(`Computer has chosen <span class="text-computer">${computerChoice.shape}</span>
                                  ${winner != 'tie' ? `and the winner is <span class="text-winner">${winner}</span>` : `and it's <span class="text-winner">tie</span>`}`, false);
    }, 1000); 
    this.updateScore(winner);
  }

  updateScore(winner) {
    this.state.score[winner] != undefined ? this.state.score[winner]++ : null;
    GameDOM.updateScoreOnScreen(this.state.score);
  }

  findWinner(computerChoice, playerShape) {
    for(let shape of computerChoice.beats) {
      if(shape == playerShape) {
        return 'computer';
      }
    }
    return computerChoice.shape != playerShape ? 'player' : 'tie';
  }

  generateComputerAnswer() {
    let numberOfOptions = this.rules.length;
    return Math.floor(Math.random()* (numberOfOptions - 0) + 0 );
  }

  resetGame() {
    this.state.score.computer = 0;
    this.state.score.player = 0;
    GameDOM.updateScoreOnScreen(this.state.score);
  }

  generateComputerGame() {
    let playerRandomChoice = this.rules[this.generateComputerAnswer()];
    this.play(playerRandomChoice.shape);
    GameDOM.updateMessageContainer(`Your choice is <span class="text-player">${playerRandomChoice.shape}</span>`);
  }
}