require('../css/main.scss');
require('../css/game.scss');

import Game from './game';

window.onload = function(){
  const rules = [{shape: "Rock", beats: ["Scissors"]},
      {shape: "Scissors", beats: ["Paper"]},
      {shape: "Paper", beats: ["Rock"]}];

  let game = new Game(rules);
  game.start();
}
