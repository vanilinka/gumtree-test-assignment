export default class GameRenderer {
  
  static subscribeToEvents(eventHandlers) {
    const resetGame = document.getElementById('resetGame');
    const computerGame = document.getElementById('computerGame');

    this.playerShapes.addEventListener('click', function(ev) {
      if(ev.target.classList.contains('game-btn')) {
        eventHandlers.play(ev.target.innerHTML);
        this.updateMessageContainer(`Your choice is <span class="text-player">${ev.target.innerHTML}</span>`);
      }
    }.bind(this));

    resetGame.addEventListener('click', function() {
      eventHandlers.resetGame();
    });

    computerGame.addEventListener('click', function() {
      eventHandlers.generateComputerGame();
    });
  }

  static renderOnScreen(rules) {
    for(const rule of rules) {
      let newShape = document.createElement("button"); 
      let shapeName = document.createTextNode(rule.shape);
      newShape.appendChild(shapeName);
      newShape.className = "game-btn";
      this.playerShapes.appendChild(newShape);
    }

    let startGameText = "To play, press on one of the shapes and wait for the computer to choose its shape."
    this.updateMessageContainer(startGameText);
  }

  static updateScoreOnScreen(score) {
    const scorePlayer = document.getElementById('scorePlayer');
    const scoreComputer = document.getElementById('scoreComputer');

    scorePlayer.innerHTML = score.player;
    scoreComputer.innerHTML = score.computer;
  }

  static updateMessageContainer(updateText, replaceText = true) {
    const messageContainer = document.getElementById('messageContainer');
    if(replaceText) {
      messageContainer.innerHTML = updateText;
    }
    else {
      messageContainer.innerHTML = messageContainer.innerHTML + '<br>' + updateText;
    }
  }
}

GameRenderer.playerShapes = document.getElementById('playerShapes');