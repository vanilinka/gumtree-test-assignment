Gumtree UK frontend developer test

## Technologies used:
- Vanilla Javascript with ES6 features
- Sass

## Features:
- Code is extendable, and we can add Lizard and Spock (tests are included).
- Computer can play on behalf of the player

## Implementation
- Game is responsive and can be played with a keyboard
- Image credit - https://buenavibra.es/wp-content/uploads/2016/11/piedra-papel-tijera-p_opt.jpg
